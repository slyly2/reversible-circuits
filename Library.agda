open import Data.Bool using (Bool; true; false; not; _∨_; _∧_)
open import Data.List
open import Function using (_∘_; _$_; id)

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; cong; sym; cong-app)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _∎; step-≡)

open import Membership

module Library where

postulate
  extensionality : ∀ {A B : Set} {f g : A → B}
    → (∀ (x : A) → f x ≡ g x)
      -----------------------
    → f ≡ g

∈-true-in-list : (l : List Bool) → true ∈ l → or l ≡ true
∈-true-in-list (true ∷ _) here = refl
∈-true-in-list (false ∷ t) (there t∈t) = ∈-true-in-list t t∈t
∈-true-in-list (true ∷ t) (there t∈t) = refl

or-refl : (b : Bool) → b ∨ b ≡ b
or-refl false = refl
or-refl true = refl

de-morgan-1 : (p q : Bool) → (not p) ∧ (not q) ≡ not (p ∨ q)
de-morgan-1 false false = refl
de-morgan-1 false true = refl
de-morgan-1 true false = refl
de-morgan-1 true true = refl

de-morgan-2 : (p q : Bool) → (not p) ∨ (not q) ≡ not (p ∧ q)
de-morgan-2 false false = refl
de-morgan-2 false true = refl
de-morgan-2 true false = refl
de-morgan-2 true true = refl

not-not : (b : Bool) → not (not b) ≡ b
not-not false = refl
not-not true = refl


map-map : ∀ {α} {A B C : Set α} → (f : B → C) → (g : A → B) → (l : List A)
  → map f (map g l) ≡ map (f ∘ g) l
map-map f g [] = refl
map-map f g (x ∷ l) = cong ((f (g x))∷_) (map-map f g l)
