open import Data.Nat using (ℕ; zero; suc)
open import Data.Vec using (Vec; []; _∷_; lookup) renaming (map to vmap)
open import Data.List using (List; []; _∷_; _++_; boolFilter; allFin; and; or) renaming (map to lmap)
open import Data.Bool using (Bool; true; false; not; _∨_; _∧_)
open import Data.Fin using (Fin; fromℕ; toℕ; zero; suc) 
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Function using (_∘_; _$_)

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; cong; sym)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _∎; _≡⟨_⟩_)

open import Membership
open import Circuit 
open import Semantics

module Comple where

comple : {n : ℕ} → (f : Vec Bool n → Bool) → Circuit n

permutations : (n : ℕ) → List (Vec Bool n)

{-
truth-table : {n : ℕ} → List (Vec Bool n) → (Vec Bool n → Bool) → List ((Vec Bool n) × Bool)

filter-true : {n : ℕ} → List ((Vec Bool n) × Bool) → List (Vec Bool n)
-}

filter-true : {n : ℕ} → (Vec Bool n → Bool) → List (Vec Bool n) → List (Vec Bool n)

integrate : {n : ℕ} → List (Vec Bool n) → Circuit n

---------------------
-- ??
---------------------

comple {n} f = integrate $ filter-true f (permutations n)

{- permutations : (n : ℕ) → List (Vec Bool n) -}
permutations zero = [] ∷ []
permutations (suc n) = lmap (λ ls → false ∷ ls) bss ++ lmap (λ ls → true ∷ ls) bss
                            where bss = permutations n

permutations-property : {n : ℕ} → (v : Vec Bool n) → v ∈ (permutations n)
permutations-property {zero} [] = here
permutations-property {suc n} (false ∷ v) = ∈-app (false ∷ v) (lmap (_∷_ false) (permutations n)) (lmap (_∷_ true) (permutations n)) (inj₁ (∈-map v (_∷_ false) (permutations n) (permutations-property v)))
permutations-property {suc n} (true ∷ v) = ∈-app (true ∷ v) (lmap (_∷_ false) (permutations n)) (lmap (_∷_ true) (permutations n)) (inj₂ (∈-map v (_∷_ true) (permutations n) (permutations-property v)))

filter-true f l = boolFilter f l
filter-true-property : {n : ℕ} (v : Vec Bool n) → (f : Vec Bool n → Bool) → (l : List (Vec Bool n))
  → v ∈ l
  → f v ≡ true 
  → v ∈ filter-true f l
filter-true-property v f l v∈l fv = ∈-filter f v l fv v∈l

---- construct the row ----

{-
e.g. ([true, false, false]) -> (suc zero) -> neg (var (suc zero))
-}


circuit-var : {n : ℕ} → (v : Vec Bool n) → (i : Fin n) → Circuit n
circuit-var v i with lookup v i
...                | true = var i
...                | false = negc (var i)

eqb : Bool → Bool → Bool
eqb true true = true
eqb false false = true
eqb _ _ = false

eqb-property1 : (b : Bool) → b ≡ eqb true b
eqb-property1 false = refl
eqb-property1 true = refl

eqb-property2 : (b : Bool) → not b ∨ not b ≡ eqb false b
eqb-property2 false = refl
eqb-property2 true = refl

circuit-var-property : {n : ℕ} → (v w : Vec Bool n) → (i : Fin n) → eval (circuit-var v i) w ≡ eqb (lookup v i) (lookup w i)
circuit-var-property {n} v w i with lookup v i
... | true = eqb-property1 (lookup w i)
... | false = eqb-property2 (lookup w i)

and-circuits : {n : ℕ} → List (Circuit n) → Circuit n
and-circuits {n} [] = const true
and-circuits {n} (x ∷ c) = andc x (and-circuits c)

and-circuits-property : {n : ℕ} {l : List (Circuit n)} {v : Vec Bool n} → and (lmap (λ c → eval c v) l) ≡ eval (and-circuits l) v
and-circuits-property {n} {[]} {v} = refl
and-circuits-property {n} {h ∷ t} {v} = {!!}

{-
Goal: and (eval h v ∷ lmap (λ c → eval c v) t) ≡
      not (not (eval h v) ∨ not (eval (and-circuits t) v)) ∨
      not (not (eval h v) ∨ not (eval (and-circuits t) v))

Goal: and (eval h v ∷ lmap (λ c → eval c v) t) ≡
      (eval h v) ∧ (eval (and-circuits t) v))

destruct (eval h v)
- (* true *)
  Goal: true ∧ (lmap (λ c → eval c v) t) ≡ true ∧ (eval (and-circuits t) v)
  Solved by induction 

- (* false *)
  Goal : false ∧ ... ≡ false ∧ ...
  simpl. refl.
-}

circuit-row : {n : ℕ} → (Vec Bool n) → Circuit n
circuit-row {n} v = and-circuits $ lmap (circuit-var v) (allFin n)

eqvec : {n m : ℕ} → (v : Vec Bool n) → (w : Vec Bool m) → Bool
eqvec [] [] = true
eqvec [] (y ∷ w) = false
eqvec (x ∷ v) [] = false
eqvec (x ∷ v) (y ∷ w) = (eqb x y) ∧ (eqvec v w)

circuit-row-property : {n : ℕ} → (v w : Vec Bool n) → eval (circuit-row v) w ≡ eqvec v w
circuit-row-property [] [] = refl
circuit-row-property (x ∷ v) (y ∷ w) with eqb x y
... | true = {!!}
{-
Goal: not
      (not (eval (circuit-var (x ∷ v) Data.Fin.0F | x) (y ∷ w)) ∨
       not
       (eval
        (and-circuits
         (lmap (λ i → circuit-var (x ∷ v) i | lookup (x ∷ v) i)
          (Data.List.tabulate suc)))
        (y ∷ w)))
      ∨
      not
      (not (eval (circuit-var (x ∷ v) Data.Fin.0F | x) (y ∷ w)) ∨
       not
       (eval
        (and-circuits
         (lmap (λ i → circuit-var (x ∷ v) i | lookup (x ∷ v) i)
          (Data.List.tabulate suc)))
        (y ∷ w)))
      ≡ eqvec v w

Goal: 
      (eval (circuit-var (x ∷ v) Data.Fin.0F | x) (y ∷ w) ∧
      (eval
        (and-circuits
         (lmap (λ i → circuit-var (x ∷ v) i | lookup (x ∷ v) i)
          (Data.List.tabulate suc)))
        (y ∷ w)))
      ≡ eqvec v w

reminder: circuit-var-property : {n : ℕ} → (v w : Vec Bool n) → (i : Fin n) → eval (circuit-var v i) w ≡ eqb (lookup v i) (lookup w i)
rewrite circuit-var-property
  eqb (lookup (x ∷ v) zero) (lookup (y ∷ w) zero) ∧ ...

simpl.
  true ∧ ...

simpl. 
current Goal:
      (eval
        (and-circuits
         (lmap (λ i → circuit-var (x ∷ v) i | lookup (x ∷ v) i)
          (Data.List.tabulate suc)))
        (y ∷ w)))
      ≡ eqvec v w

We have: eval (circuit-row v) w ≡ eqvec v w
Reminder: and-circuits-property : and (lmap (λ c → eval c v) l) ≡ eval (and-circuits l) v

rewrite and-circuits-property
and (lamp (λ c → eval c (y ∷ w)) (lmap (λ i → circuit-var (x ∷ v) i | lookup (x ∷ v) i) (Data.List.tabulate suc)))) 
≡ eqvec v w

Abort....
maybe I need some lemmas

-}

... | false = {!!}
-----------------------

---- construct the whole circuit ----
or-circuits : {n : ℕ} → List (Circuit n) → Circuit n
or-circuits [] = const false
or-circuits (x ∷ l) = orc x (or-circuits l)

or-vecbool : {n : ℕ} → (v : Vec Bool n) → Bool
or-vecbool [] = false
or-vecbool (x ∷ v) = x ∨ or-vecbool v

or-circuits-property : {n : ℕ} →  (l : List (Circuit n)) → (v : Vec Bool n) → or (lmap (λ c → eval c v) l) ≡ eval (or-circuits l) v 
or-circuits-property = {!!}
{- the proof is similar to the proof of and-circuits-property -}

{- integrate : {n : ℕ} → List (Vec Bool n) → Circuit n -}
integrate = or-circuits ∘ lmap (circuit-row)
