open import Data.Nat using (ℕ; zero; suc; _+_; _≥_)
open import Data.Vec using (Vec; []; _∷_; lookup; foldr₁) renaming (map to vmap)
open import Data.List using (List; []; _∷_; _++_; filter; allFin; and; or) renaming (map to lmap)
open import Data.Bool using (Bool; true; false; not; _∨_; _∧_)
open import Data.Fin using (Fin; fromℕ; toℕ) renaming (zero to fzero; suc to fsuc)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Function using (_∘_; _$_)

open import Membership
open import Circuit 
open import Semantics

module Completeness where


---------------------
-- Structure
---------------------

comple : {n : ℕ} → (f : Vec Bool n → Bool) → Circuit n

permutations : (n : ℕ) → List (Vec Bool n)

truth-table : {n : ℕ} → List (Vec Bool n) → (Vec Bool n → Bool) → List ((Vec Bool n) × Bool)

filter-true : {n : ℕ} → List ((Vec Bool n) × Bool) → List (Vec Bool n)

integrate : {n : ℕ} → List (Vec Bool n) → Circuit n

---------------------
-- ??
---------------------

comple {n} f = integrate ∘ filter-true $ truth-table (permutations n) f

--literate agda .lagda 
{- permutations : (n : ℕ) → List (Vec Bool n) -}
permutations zero = [] ∷ []
permutations (suc n) = lmap (λ ls → false ∷ ls) bss ++ lmap (λ ls → true ∷ ls) bss
                            where bss = permutations n

permutations-property : {n : ℕ} → (v : Vec Bool n) → v ∈ (permutations n)
{-
permutations-property {Data.Fin.0F} [] = here
permutations-property {suc n} (false ∷ v) = ∈-app (false ∷ v) (lmap (_∷_ false) (permutations n)) (lmap (_∷_ true) (permutations n)) (inj₁ (∈-map v (_∷_ false) (permutations n) (permutations-property v)))
permutations-property {suc n} (true ∷ v) = ∈-app (true ∷ v) (lmap (_∷_ false) (permutations n)) (lmap (_∷_ true) (permutations n)) (inj₂ (∈-map v (_∷_ true) (permutations n) (permutations-property v)))
-}
{- truth-table : {n : ℕ} → List (Vec Bool n) → (Vec Bool n → Bool) → List ((Vec Bool n) × Bool) -}
truth-table l f = lmap (λ r → r , f r) l

{- filter-true : {n : ℕ} → List ((Vec Bool n) × Bool) → List (Vec Bool n) -}
filter-true [] = []
filter-true ((fst , false) ∷ l) = filter-true l
filter-true ((fst , true) ∷ l) = fst ∷ (filter-true l)

truth-table-property : {n : ℕ} {f : Vec Bool n → Bool} {v : Vec Bool n} {l : List (Vec Bool n)}
  → f v ≡ true
  → v ∈ l
  → v ∈ (filter-true (truth-table l f))
truth-table-property {n} {f} {v} {.(v ∷ _)} fv here = {!!}
truth-table-property {n} {f} {v} {.(_ ∷ _)} fv (there v∈l) = {!!}

---- construct the row ----

{-
e.g. ([true, false, false]) -> (suc zero) -> neg (var (suc zero))
-}


circuit-var : {n : ℕ} → (v : Vec Bool n) → (i : Fin n) → Circuit n
circuit-var v i with lookup v i
...                | true = var i
...                | false = negc (var i)
{-
test : Bool
test = eval (circuit-var (false ∷ []) 0F) (false ∷ [])
-- true
-}
-- lookup (false ∷ []) 0F -> false

-- eval (var 0F) (false ∷ []) -> false
{-
test1 : Circuit Data.Fin.1F
test1 = circuit-var (false ∷ []) 0F
-- nand (var 0F) (var 0F)
-}

eqb : Bool → Bool → Bool
eqb x y = {!!}

circuit-var-property : {n : ℕ} → (v w : Vec Bool n) → (i : Fin n) → eval (circuit-var v i) w ≡ eqb (lookup v i) (lookup w i) 
--eval (circuit-var v i) v ≡ lookup v i
circuit-var-property v i = {!!}
{- lookup v i = false
-}

-- v = 010
-- w = 110
--     011

{-
circuit-var-property {Data.Fin.1F} (false ∷ []) 0F = {!!}
circuit-var-property {Data.Fin.1F} (true ∷ []) 0F = {!!}
circuit-var-property {suc (suc n)} v i = {!!}
-}
and-circuits : {n : ℕ} → List (Circuit n) → Circuit n
and-circuits {n} [] = const true
and-circuits {n} (x ∷ c) = andc x (and-circuits c)

and-circuits-property : {n : ℕ} {l : List (Circuit n)} {v : Vec Bool n} → and (lmap (λ c → eval c v) l) ≡ eval (and-circuits l) v
and-circuits-property = {!!}

-- v = 010
-- w = 010 -> 1
-- w = 11

circuit-row : {n : ℕ} → (Vec Bool n) → Circuit n
circuit-row {n} v = and-circuits $ lmap (circuit-var v) (allFin n)

and-vecbool : {n : ℕ} → (v : Vec Bool n) → Bool
and-vecbool [] = true
and-vecbool (x ∷ v) = x ∧ and-vecbool v

circuit-row-property : {n : ℕ} {v : Vec Bool n} → and-vecbool v ≡ eval (circuit-row v) v
-----------------------

---- construct the whole circuit ----
or-circuits : {n : ℕ} → List (Circuit n) → Circuit n
or-circuits [] = const false
or-circuits (x ∷ l) = orc x (or-circuits l)

or-vecbool : {n : ℕ} → (v : Vec Bool n) → Bool
or-vecbool [] = false
or-vecbool (x ∷ v) = x ∨ or-vecbool v

or-circuits-property : {n : ℕ} {l : List (Circuit n)} {v : Vec Bool n} → or (lmap (λ c → eval c v) l) ≡ eval (or-circuits l) v

{- integrate : {n : ℕ} → List (Vec Bool n) → Circuit n -}
integrate = or-circuits ∘ lmap (circuit-row)

-- integrate-property : ???
-------------------------------------

{-
approach1 : from top to bottom
-}

lemma11 : {n : ℕ} → (l : List (Circuit n)) → (v : Vec Bool n)
  → (circuit-row v) ∈ l
  → (or (lmap (λ c → eval c v) l)) ≡ true
lemma11 l v cv∈l = {!!}


lemma1 : {n : ℕ} → (l : List (Circuit n)) → (v : Vec Bool n)
  → (circuit-row v) ∈ l
  → (eval (or-circuits l) v) ≡ true
lemma1 l v cv∈l = {!!}

-- can be abstracted
lemma21 : {n : ℕ} → (l : List (Vec Bool n)) → (v : Vec Bool n)
  → v ∈ l
  → (circuit-row v) ∈ (lmap circuit-row l)
lemma21 l v v∈l = ∈-map v circuit-row l v∈l


lemma2 : {n : ℕ} → (f : Vec Bool n → Bool) → (v : Vec Bool n)
  → (f v ≡ true)
  → (circuit-row v) ∈ (lmap circuit-row (filter-true (truth-table (permutations n) f)))
lemma2 {n} f v fv = lemma21 ((filter-true (truth-table (permutations n) f))) v (truth-table-property fv (permutations-property v))
-- lemma21 (truth-table-property fv (permutations-property v))


-- comple : {n : ℕ} → (f : Vec Bool n → Bool) → Circuit n
-- comple {n} f = integrate ∘ filter-true $ truth-table (permutations n) f

comple-ok : {n : ℕ} → (f : Vec Bool n → Bool) → (v : Vec Bool n) → eval (comple f) v ≡ f v
comple-ok f [] with f []
... | true = refl
... | false = refl
comple-ok {n} f v with f v
... | true = lemma1 (lmap circuit-row (filter-true $ truth-table (permutations n) f)) v (lemma2 f v {!!})
... | false = {!!}

{-
approach2 : from bottom to top
-}






