import Relation.Binary.PropositionalEquality as Eq
open Eq 

open import Data.Nat using (ℕ; zero; suc)
open import Data.Vec using (Vec; []; _∷_; lookup; tabulate)
open import Data.Bool using (Bool; true; false)
open import Data.Fin using (Fin; toℕ) renaming (zero to fz; suc to fs)

open import Library

module Isomorphism where

infix 0 _≃_
record _≃_ (A B : Set) : Set where
  field
    to      : A → B
    from    : B → A
    from∘to : ∀ (x : A) → from (to x) ≡ x
    to∘from : ∀ (y : B) → to (from y) ≡ y
open _≃_

vecbooln-to-finn : {n : ℕ} → (Vec Bool n) → (Fin n → Bool)
vecbooln-to-finn = lookup

finn-to-vecbooln : {n : ℕ} → (Fin n → Bool) → (Vec Bool n)
finn-to-vecbooln = tabulate

from∘to-helper : {n : ℕ} → (x : Vec Bool n) → tabulate (lookup x) ≡ x
from∘to-helper [] = refl
from∘to-helper (h ∷ t) =  cong (h ∷_) (from∘to-helper t)

to∘from-helper : {n : ℕ} → (y : Fin n → Bool) → lookup (tabulate y) ≡ y
to∘from-helper {zero} y = extensionality λ ()
to∘from-helper {suc n} y = extensionality {!!}

vecbooln≃finn : {n : ℕ} → (Vec Bool n) ≃ (Fin n → Bool)
vecbooln≃finn =
  record
    { to      = lookup
    ; from    = tabulate
    ; from∘to = from∘to-helper
    ; to∘from = λ y → {!!}
    }

exn : Fin 4 → ℕ
exn f = toℕ f 
