open import Data.Nat using (ℕ)
open import Data.Bool using (Bool; true; false; not; _∧_; _∨_)
open import Data.Vec
open import Data.Product
open import Data.List using (List; []; _∷_; map)
open import Data.Fin using (Fin; zero; suc)

open import Function

open import Circuit 
open import Semantics

module Example where

ex1 : Circuit 3
ex1 = var (suc (suc zero))

ex2 : Circuit 2
ex2 = nand (var zero)
           (var (suc zero))

ex3 : Bool
ex3 = eval ex2 (true ∷ false ∷ [])
-- ex3 = true

fun1 : Vec Bool 3 → Bool
fun1 (x₀ ∷ x₁ ∷ x₂ ∷ []) = x₀ ∧ ((not x₁) ∧ x₂)
