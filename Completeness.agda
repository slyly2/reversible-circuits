open import Data.Nat using (ℕ; zero; suc)
open import Data.Vec using (Vec; []; _∷_; lookup) renaming (map to vmap)
open import Data.List using (List; []; _∷_; _++_; boolFilter; allFin; and; or; all; tabulate; foldr) renaming (map to lmap)
open import Data.Bool using (Bool; true; false; not; _∨_; _∧_)
open import Data.Fin using (Fin; fromℕ; toℕ; zero; suc) 
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Function using (_∘_; _$_; id)

import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; cong; sym; cong-app)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _∎; step-≡)

open import Membership
open import Singleton
open import Circuit 
open import Semantics

open import Library

module Completeness where


---------------------
-- Structure
---------------------

comple : {n : ℕ} → (f : Vec Bool n → Bool) → Circuit n

permutations : (n : ℕ) → List (Vec Bool n)

{-
truth-table : {n : ℕ} → List (Vec Bool n) → (Vec Bool n → Bool) → List ((Vec Bool n) × Bool)

filter-true : {n : ℕ} → List ((Vec Bool n) × Bool) → List (Vec Bool n)
-}

filter-true : {n : ℕ} → (Vec Bool n → Bool) → List (Vec Bool n) → List (Vec Bool n)

integrate : {n : ℕ} → List (Vec Bool n) → Circuit n

---------------------
-- ??
---------------------

comple {n} f = integrate $ filter-true f (permutations n)

{- permutations : (n : ℕ) → List (Vec Bool n) -}
permutations zero = [] ∷ []
permutations (suc n) = lmap (λ ls → false ∷ ls) bss ++ lmap (λ ls → true ∷ ls) bss
                            where bss = permutations n

permutations-property : {n : ℕ} → (v : Vec Bool n) → v ∈ (permutations n)
permutations-property {zero} [] = here
permutations-property {suc n} (false ∷ v) = ∈-app (false ∷ v) (lmap (_∷_ false) (permutations n)) (lmap (_∷_ true) (permutations n)) (inj₁ (∈-map v (_∷_ false) (permutations n) (permutations-property v)))
permutations-property {suc n} (true ∷ v) = ∈-app (true ∷ v) (lmap (_∷_ false) (permutations n)) (lmap (_∷_ true) (permutations n)) (inj₂ (∈-map v (_∷_ true) (permutations n) (permutations-property v)))

filter-true f l = boolFilter f l
filter-true-property : {n : ℕ} (v : Vec Bool n) → (f : Vec Bool n → Bool) → (l : List (Vec Bool n))
  → v ∈ l
  → f v ≡ true 
  → v ∈ filter-true f l
filter-true-property v f l v∈l fv = ∈-filter f v l fv v∈l

filter-true-property2 : {n : ℕ} (v : Vec Bool n) → (f : Vec Bool n → Bool) → (l : List (Vec Bool n))
  → v ∈ l
  → f v ≡ false
  → v ∉ filter-true f l
filter-true-property2 v f l v∈l fv = {!!}

---- construct the row ----

{-
e.g. ([true, false, false]) -> (suc zero) -> neg (var (suc zero))
-}


circuit-var : {n : ℕ} → (v : Vec Bool n) → (i : Fin n) → Circuit n
circuit-var v i with lookup v i
...                | true = var i
...                | false = negc (var i)

eqb : Bool → Bool → Bool
eqb true true = true
eqb false false = true
eqb _ _ = false

eqb-property1 : (b : Bool) → b ≡ eqb true b
eqb-property1 false = refl
eqb-property1 true = refl

eqb-property2 : (b : Bool) → not b ∨ not b ≡ eqb false b
eqb-property2 false = refl
eqb-property2 true = refl

circuit-var-property : {n : ℕ} → (v w : Vec Bool n) → (i : Fin n) → eval (circuit-var v i) w ≡ eqb (lookup v i) (lookup w i)
circuit-var-property {n} v w i with lookup v i
... | true = eqb-property1 (lookup w i)
... | false = eqb-property2 (lookup w i)

and-circuits : {n : ℕ} → List (Circuit n) → Circuit n
and-circuits {n} [] = const true
and-circuits {n} (x ∷ c) = andc x (and-circuits c)

and-circuits-property : {n : ℕ} → (l : List (Circuit n)) → (v : Vec Bool n) → and (lmap (λ c → eval c v) l) ≡ eval (and-circuits l) v
and-circuits-property {n} [] v = refl
and-circuits-property {n} (h ∷ t) v
  rewrite or-refl (not (not (eval h v) ∨ not (eval (and-circuits t) v)))
        | sym (de-morgan-1 (not (eval h v)) (not (eval (and-circuits t) v)))
        | not-not (eval h v) | not-not (eval (and-circuits t) v) with (eval h v)
... | true = and-circuits-property t v
... | false = refl

circuit-row : {n : ℕ} → (Vec Bool n) → Circuit n
circuit-row {n} v = and-circuits $ lmap (circuit-var v) (allFin n)

eqvec : {n m : ℕ} → (v : Vec Bool n) → (w : Vec Bool m) → Bool
eqvec [] [] = true
eqvec [] (y ∷ w) = false
eqvec (x ∷ v) [] = false
eqvec (x ∷ v) (y ∷ w) = (eqb x y) ∧ (eqvec v w)

eqvec-refl : {n : ℕ} → (v : Vec Bool n) → eqvec v v ≡ true
eqvec-refl [] = refl
eqvec-refl (false ∷ v) = eqvec-refl v
eqvec-refl (true ∷ v) = eqvec-refl v

{-
interim-lemma : {n : ℕ} → (v w : Vec Bool n)
  →  and ( lmap (λ c → eval c w) (lmap (circuit-var v) (allFin n)) ) ≡ and ( lmap (λ i → eval (circuit-var v i) w) (allFin n) )
interim-lemma {n} v w = cong and (map-map (λ c → eval c w) (circuit-var v) (allFin n))
-- map f (map g l) ≡ map f∘g l

interim-lemma2 : {n : ℕ} → (v w : Vec Bool n)
  → and ( lmap (λ i → eval (circuit-var v i) w) (allFin n) ) ≡  and ( lmap (λ i → eqb (lookup v i) (lookup w i)) (allFin n) )
interim-lemma2 {n} v w = cong (λ f → and (lmap f (allFin n))) (extensionality (circuit-var-property v w))
-}

lookup-property : ∀ {A : Set} {n : ℕ} → (v : Vec A n) → (x : A) → (i : Fin n)
  → lookup v i ≡ lookup (x ∷ v) (suc i)
lookup-property v x i = refl

lemma4 : {n : ℕ} → (v w : Vec Bool n) → (b : Bool)
  → lmap (λ i → eqb (lookup v i) (lookup w i)) (tabulate id)
  ≡ lmap (λ i → eqb (lookup (b ∷ v) i) (lookup (b ∷ w) i)) (tabulate suc)
lemma4 {zero} v w b = refl
lemma4 {suc n} v w b = cong ((eqb (lookup v zero) (lookup w zero))∷_) {!!}


interim-lemma3 : {n : ℕ} → (v w : Vec Bool n)
  → eqvec v w ≡ and ( lmap (λ i → eqb (lookup v i) (lookup w i)) (allFin n) )
interim-lemma3 [] [] = refl
interim-lemma3 (false ∷ v) (false ∷ w) rewrite sym (lemma4 v w false) = interim-lemma3 v w
interim-lemma3 (false ∷ v) (true ∷ w) = refl
interim-lemma3 (true ∷ v) (false ∷ w) = refl
interim-lemma3 (true ∷ v) (true ∷ w) rewrite sym (lemma4 v w true) = interim-lemma3 v w

circuit-row-property : {n : ℕ} → (v w : Vec Bool n) → eval (circuit-row v) w ≡ eqvec v w
circuit-row-property {n} v w =
  begin
    eval (circuit-row v) w
  ≡⟨⟩
    eval ( and-circuits (lmap (circuit-var v) (allFin n)) ) w
  ≡⟨ sym (and-circuits-property (lmap (circuit-var v) (allFin n)) w) ⟩
    and ( lmap (λ c → eval c w) (lmap (circuit-var v) (allFin n)) )
  ≡⟨ cong and (map-map (λ c → eval c w) (circuit-var v) (allFin n)) ⟩ -- map f (map g l) ≡ map f∘g l
    and ( lmap (λ i → eval (circuit-var v i) w) (allFin n) )
  ≡⟨ cong (λ f → and (lmap f (allFin n))) (extensionality (circuit-var-property v w)) ⟩
    and ( lmap (λ i → eqb (lookup v i) (lookup w i)) (allFin n) )
  ≡⟨ sym (interim-lemma3 v w) ⟩
    eqvec v w
  ∎

{-
  eval (circuit-row v) w

  eval ( and-circuits (lmap (circuit-var v) (allFin n)) ) w

  and ( lmap (λ c → eval c w) (lmap (circuit-var v) (allFin n)) )
rewrite 
  and ( lmap (λ i → eval (circuit-var v i) w) (allFin n) )

--------------
  eqvec v w
  
  and ( lmap (λ i → eqb (lookup v i) (lookup w i)) (allFin n) )
-}
-----------------------

---- construct the whole circuit ----
or-circuits : {n : ℕ} → List (Circuit n) → Circuit n
or-circuits [] = const false
or-circuits (x ∷ l) = orc x (or-circuits l)

or-vecbool : {n : ℕ} → (v : Vec Bool n) → Bool
or-vecbool [] = false
or-vecbool (x ∷ v) = x ∨ or-vecbool v

or-circuits-property : {n : ℕ} → (l : List (Circuit n)) → (v : Vec Bool n) → eval (or-circuits l) v ≡ or (lmap (λ c → eval c v) l)
or-circuits-property [] v = refl
or-circuits-property (h ∷ t) v
  rewrite or-refl (not (eval h v)) | or-refl (not (eval (or-circuits t) v))
        | or-refl (not (not (not (eval h v)) ∨ not (not (eval (or-circuits t) v))))
        | or-refl (not (not (not (not (eval h v)) ∨ not (not (eval (or-circuits t) v)))))
        | not-not (not (not (eval h v)) ∨ not (not (eval (or-circuits t) v)))
        | sym (de-morgan-1 (not (eval h v)) (not (not (eval (or-circuits t) v))))
        | not-not (eval h v) | not-not (eval (or-circuits t) v) with eval h v
... | true = refl
... | false = or-circuits-property t v

{- integrate : {n : ℕ} → List (Vec Bool n) → Circuit n -}
integrate = or-circuits ∘ lmap (circuit-row)
-------------------------------------

lemma11 : {n : ℕ} {l : List (Circuit n)} {v : Vec Bool n}
  → (circuit-row v) ∈ l
  → (eval (circuit-row v) v) ∈ (lmap (λ f → (eval f v)) l)
lemma11 {n} {l} {v} cv∈l = ∈-map ((circuit-row v)) ((λ f → (eval f v))) l cv∈l

lemma12 : {n : ℕ} {l : List (Circuit n)} {v : Vec Bool n}
  → (eval (circuit-row v) v) ∈ (lmap (λ f → (eval f v)) l)
  → or (lmap (λ f → (eval f v)) l) ≡ true
lemma12 {n} {l} {v} ∈ rewrite circuit-row-property v v | eqvec-refl v = ∈-true-in-list (lmap (λ f → eval f v) l) ∈

lemma1 : {n : ℕ} → (l : List (Circuit n)) → (v : Vec Bool n)
  → (circuit-row v) ∈ l
  → (eval (or-circuits l) v) ≡ true
lemma1 l v cv∈l rewrite or-circuits-property l v = lemma12 (lemma11 cv∈l)

{-
using ∈-map
  (circuit-row v) ∈ l → (λ f → (eval f v)) (circuit-row v) ∈ (map (λ f → (eval f v)) l)

simplify it, we can get
  (eval (circuit-row v) v) ∈ (map (λ f → (eval f v)) l)

rewrite circuit-row-property
  eqvec v v ∈ (map (λ f → (eval f v)) l)

eqvec v v ≡ true
  true ∈ (map (λ f → (eval f v)) l)

forall (l : list bool), true ∈ l → or l ≡ true
  (eval (λ c → c v)) (circuit-row v) ∈ (map (eval (λ c → c v)) l)

in the true branch 

a ∈ as

circuit-row v ∈ l

f a ∈ map f as



circuit-row v ∉ l

true ∉ [false]

f : Boolean → Boolean 
f _ = false

map f x ∉ map f xs

a ∉ as




Finish!
-}

-- can be abstracted
lemma21 : {n : ℕ} {l : List (Vec Bool n)} {v : Vec Bool n}
  → v ∈ l
  → (circuit-row v) ∈ (lmap circuit-row l)
lemma21 {n} {l} {v} v∈l = ∈-map v circuit-row l v∈l

lemma2 : {n : ℕ} {f : Vec Bool n → Bool} {v : Vec Bool n}
  → (f v ≡ true)
  → (circuit-row v) ∈ (lmap circuit-row (filter-true f (permutations n)))
lemma2 {n} {f} {v} fv = lemma21 (filter-true-property v f (permutations n) (permutations-property v) fv)

{-
x ∉ a ∷ l → x ≠ a ∧ x ∉ l

x ∉ l → x ≠ a → x ∉ a ∷ l
-}

lemma1'1 : {p q : Bool}
  → p ≡ false
  → q ≡ false
  → p ∨ q ≡ false
lemma1'1 {false} {false} = λ _ _ → refl
lemma1'1 {false} {true} = λ _ z → z
lemma1'1 {true} {false} = λ z _ → z
lemma1'1 {true} {true} = λ _ z → z

helpl : (p q : Bool)
  →  not
      (not (not (not p ∨ not p) ∨ not (not q ∨ not q))
       ∨
       not (not (not p ∨ not p ) ∨ not (not q ∨ not q)))
      ∨
      not
      (not (not (not p ∨ not p) ∨ not (not q ∨ not q ))
       ∨
       not (not (not p ∨ not p) ∨ not (not q ∨ not q ))) ≡  p ∨ q
helpl false false = refl
helpl false true = refl
helpl true false = refl
helpl true true = refl

postulate
  subst-back : ∀ {A B} {x y : A} (f : A → B)
    → f x ≢ f y
      ---------
    → x ≢ y

  neq-sym : ∀ {A} {x y : A}
    → x ≢ y
      -----
    → y ≢ x

prop2boolf : {n : ℕ} → (v w : Vec Bool n)
  → v ≢ w
  → eqvec v w ≡ false
prop2boolf = {!!}

lemma1' : {n : ℕ} → (l : List (Vec Bool n)) → (v : Vec Bool n)
  → (circuit-row v) ∉ (lmap circuit-row l)
  → (eval (or-circuits (lmap circuit-row l)) v) ≡ false
lemma1' [] v cv∉l = refl
lemma1' (w ∷ l) v cv∉l
  rewrite helpl (eval (circuit-row w) v) (eval (or-circuits (lmap circuit-row l)) v)
        | circuit-row-property w v 
        = lemma1'1 (prop2boolf w v (subst-back circuit-row (neq-sym (proj₁ (∉-back cv∉l))))) (lemma1' l v (proj₂ (∉-back cv∉l))) 

{-
circuit-row : {n : ℕ} → (Vec Bool n) → Circuit n
circuit-row {n} v = and-circuits $ lmap (circuit-var v) (allFin n)
-}

hardest-2 : {n : ℕ} → (v w : Vec Bool n) → (b : Bool)
  → circuit-row v ≢ circuit-row w
  → circuit-row (b ∷ v) ≢ circuit-row (b ∷ w)
hardest-2 = {!!}

hardest-1 : {n : ℕ} → (v w : Vec Bool n) → (b : Bool)
  → (b ∷ v) ≢ (b ∷ w)
  → v ≢ w
hardest-1 = {!!}

hardest : {n : ℕ} {v w : Vec Bool n}
  → v ≢ w
  → circuit-row v ≢ circuit-row w
hardest {zero} {[]} {[]} v≢w = λ _ → v≢w refl
hardest {suc n} {false ∷ v} {false ∷ w} fv≢fw = hardest-2 v w false {!!}
hardest {suc n} {false ∷ v} {true ∷ w} _ = λ ()
hardest {suc n} {true ∷ v} {false ∷ w} _ = λ ()
hardest {suc n} {true ∷ v} {true ∷ w} tv≢tw = {!!}

lemma21' : {n : ℕ} {l : List (Vec Bool n)} {v : Vec Bool n}
  → v ∉ l
  → (circuit-row v) ∉ (lmap circuit-row l)
lemma21' {n} {[]} {v} v∉l = λ ()
lemma21' {n} {w ∷ l} {v} v∉wl = ∉-forward (lemma21' (proj₂ (∉-back v∉wl))) (hardest (proj₁ (∉-back v∉wl)))

lemma2' : {n : ℕ} {f : Vec Bool n → Bool} {v : Vec Bool n}
  → (f v ≡ false)
  → (circuit-row v) ∉ (lmap circuit-row (filter-true f (permutations n)))
lemma2' {n} {f} {v} fv = lemma21' (filter-true-property2 v f (permutations n) (permutations-property v) fv)

comple-ok : {n : ℕ} → (f : Vec Bool n → Bool) → (v : Vec Bool n) → eval (comple f) v ≡ f v
comple-ok {n} f v with inspect (f v)
... | true with≡ fv rewrite fv = lemma1 (lmap circuit-row (filter-true f (permutations n))) v (lemma2 fv)
... | false with≡ fv rewrite fv = lemma1' ((filter-true f (permutations n))) v (lemma2' fv)
