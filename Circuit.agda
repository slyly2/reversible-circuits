open import Data.Nat using (ℕ; _≥_)
open import Data.Fin
open import Data.Bool using (Bool)

module Circuit where

data Circuit (n : ℕ) : Set where
  const : Bool → Circuit n
  var : Fin n → Circuit n
  nand : Circuit n → Circuit n → Circuit n

negc : {n : ℕ} → Circuit n → Circuit n
negc c = nand c c

andc : {n : ℕ} → Circuit n → Circuit n → Circuit n
andc c₁ c₂ = negc (nand c₁ c₂)

orc : {n : ℕ} → Circuit n → Circuit n → Circuit n
orc c₁ c₂ = negc (andc (negc c₁) (negc c₂))

ex1 : Circuit 4
ex1 = nand
       (nand
         (nand (var zero) (var (suc zero)))
         (var (suc (suc zero))))
       (nand
         (var (suc (suc zero)))
         (var (suc (suc (suc zero)))))
