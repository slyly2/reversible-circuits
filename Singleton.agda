open import Relation.Binary.PropositionalEquality using (_≡_; refl)

module Singleton where

data Singleton {A : Set} (x : A) : Set where
  _with≡_ : (y : A) → x ≡ y → Singleton x

inspect : ∀{A : Set}(x : A) → Singleton x
inspect x = x with≡ refl
