open import Data.List using (List; boolFilter; []; _∷_; map; _++_)
open import Data.Bool using (Bool; true; false)
open import Relation.Binary.PropositionalEquality using (_≡_)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Data.Product using (_×_)
open import Relation.Nullary using (¬_)
open import Data.Empty

module Membership where

infix 4 _∈_ _∉_

data _∈_ {α} {A : Set α} (a : A) : List A → Set α where
  here : ∀ {as} → a ∈ (a ∷ as)
  there : ∀ {b as} → a ∈ as → a ∈ (b ∷ as)

_∉_ : ∀ {A : Set} (a : A) (l : List A) → Set
a ∉ l = ¬ (a ∈ l)

_≢_ : ∀ {A : Set} → A → A → Set
x ≢ y  =  ¬ (x ≡ y)

postulate
  ∉-back : ∀ {A : Set} {x y : A} {l : List A}
    → x ∉ y ∷ l
      -------------
    → x ≢ y × x ∉ l
    
  ∉-forward : ∀ {A : Set} {x y : A} {l : List A}
    → x ∉ l
    → x ≢ y
      ---------
    → x ∉ y ∷ l

∈-map : ∀ {A B : Set}  → (a : A) → (f : A → B) → (l : List A)
  → a ∈ l
  → (f a) ∈ (map f l)
∈-map a f (a ∷ _) here = here
∈-map a f (_ ∷ as) (there al) = there (∈-map a f as al)

∈-app : ∀ {A : Set} → (a : A) → (l1 l2 : List A)
  → (a ∈ l1) ⊎ (a ∈ l2)
  → a ∈ (l1 ++ l2)
∈-app a _ l2 (inj₁ here) = here
∈-app a (h ∷ t) l2 (inj₁ (there a∈t)) = there (∈-app a t l2 (inj₁ a∈t))
∈-app a [] _ (inj₂ here) = here
∈-app a (_ ∷ t1) (_ ∷ t2) (inj₂ here) = there (∈-app a t1 (a ∷ t2) (inj₂ here))
∈-app a [] l2 (inj₂ (there y)) = there y
∈-app a (_ ∷ t1) l2 (inj₂ (there y)) = there (∈-app a t1 l2 (inj₂ (there y)))

∈-filter : ∀ {A : Set} → (p : A → Bool) → (a : A) → (l : List A)
  → p a ≡ true
  → a ∈ l
  → a ∈ boolFilter p l
∈-filter p a (a ∷ _) pa here with p a
... | true = here
∈-filter p a (h ∷ t) pa (there a∈t) with p h
... | true = there (∈-filter p a t pa a∈t)
... | false = ∈-filter p a t pa a∈t


{-
∉-filter : ∀ {A : Set} → (p : A → Bool) → (a : A) → (l : List A)
  → p a ≡ false
  → a ∈ l
  → a ∉ boolFilter p l
∉-filter p a (a ∷ t) pa here a∈pt with p a
... | false = {!!}
∉-filter p a (h ∷ t) pa (there a∈t) a∈pl with p h
... | true = {!!}
... | false = {!!}
-}
