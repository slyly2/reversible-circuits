open import Data.Nat using (ℕ; _≥_)
open import Data.Bool using (Bool; not; _∧_; _∨_)
open import Data.Fin using (Fin)
open import Data.Vec using (Vec; lookup)

open import Circuit

module Semantics where

eval : {n : ℕ} → Circuit n → Vec Bool n → Bool
eval (const b) _ = b
eval (var x) v = lookup v x
eval (nand c₁ c₂) v = (not (eval c₁ v)) ∨ (not (eval c₂ v))

